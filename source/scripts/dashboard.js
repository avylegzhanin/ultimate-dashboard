import { $on, $qs, getGUID } from './modules/helpers.js';
import Template from './modules/template.js';

$on(window, 'load', () => {
  let template = new Template();

  let $input = $qs('#todo-text');
  let $addNewButton = $qs('#add-new-todo');
  let $itemsList = $qs('#todo-list');

  const test = () => {
    let itemId = getGUID();
    let itemText = $input.value;
    let itemMarkup = template.generateFor('item', {
      id: itemId,
      text: itemText
    });

    $itemsList.innerHTML += itemMarkup;
    $input.value = '';
    $input.focus();
  };

  $on($addNewButton, 'click', test);

  $on($input, 'keydown', event => {
    if (event.key === 'Enter') test();
  });
});
