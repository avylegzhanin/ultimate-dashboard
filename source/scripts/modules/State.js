const initialState = [
  {
    id: 'ffa27173-e7da-42df-59ca-12539942428f',
    text: 'First fucking task',
    completed: false
  },
  {
    id: '984fb455-380b-4e91-d6d1-1d4572a44e89',
    text: 'Second task',
    completed: false
  },
  {
    id: '8d2c751e-bb6c-4e93-9cf8-790ffc9a11de',
    text: 'Third and last one...',
    completed: false
  }
];

export default class State {
  constructor() {
    this._todos = [...initialState];
  }

  get todos() {
    return this._todos;
  }

  get count() {
    return this._todos.length;
  }

  add(itemData) {
    this._todos.push(itemData);
  }

  remove(index) {
    this._todos.splice(index, 1);
  }

  findIndex(id) {
    for (let i = 0, end = this.count; i < end; i += 1) {
      if (this._todos[i].id === id) {
        return i;
      }
    }

    return -1;
  }

  changeItemData(id, newData) {
    let index = this.findIndex(id);
    this._todos[index] = Object.assign({}, this._todos[index], { ...newData });
  }
}
