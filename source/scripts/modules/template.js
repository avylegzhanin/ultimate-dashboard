export default class Template {
  constructor() {}

  ['item']({ id, text }) {
    return `
            <li class="todo" data-id="${id}">
                <input class="todo__input" id="${id}" type="checkbox"/>
                <label class="todo__label" for="${id}"></label>
                <span class="todo__text">${text}</span>
                <button class="todo__remove">x</button>
            </li>
        `;
  }

  generateFor(templateName, data) {
    return this[templateName](data).replace(/\n\t/, '');
  }
}
