export const $on = (target, event, handler, useCapture = false) => {
  target.addEventListener(event, handler, useCapture);
};

export const $qs = (selector, scope) => {
  return (scope || document).querySelector(selector);
};

export const $qsa = (selector, scope) => {
  return (scope || document).querySelectorAll(selector);
};

function S4() {
  return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
}

export const getGUID = () => {
  return (
    S4() +
    S4() +
    '-' +
    S4() +
    '-4' +
    S4().substr(0, 3) +
    '-' +
    S4() +
    '-' +
    S4() +
    S4() +
    S4()
  ).toLowerCase();
};
