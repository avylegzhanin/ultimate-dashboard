const { src, dest, series, parallel, watch } = require('gulp');
const pug = require('gulp-pug');
const sass = require('gulp-sass');
const sync = require('browser-sync').create();

sass.compiler = require('node-sass');

const initServer = done => {
  sync.init({
    server: {
      baseDir: './dist'
    }
  });
};

const styles = done => {
  src('./source/styles/main.scss')
    .pipe(sass())
    .pipe(dest('./dist'));

  done();
};

const views = done => {
  src('./source/index.pug')
    .pipe(pug({ pretty: true }))
    .pipe(dest('./dist'));

  done();
};

const scripts = done => {
  src('./source/**/*.js').pipe(dest('./dist'));
  done();
};

const initWatchers = done => {
  watch('./source/**/*.scss', series(styles));
  watch('./source/**/*.pug', series(views));
  watch('./source/**/*.js', series(scripts));
  watch('./dist/*').on('change', series(sync.reload));
  watch('./dist/*').on('add', series(sync.reload));

  done();
};

exports.build = parallel(styles, views, scripts);
exports.start = series(
  parallel(styles, views, scripts),
  initWatchers,
  initServer
);
